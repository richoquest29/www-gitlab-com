---
layout: markdown_page
title: "TMRG - GitLab Pride"
description: "We are the GitLab Pride Team Member Resource Group (TMRG) founded in the fall of 2019. Learn more!"
canonical_path: "/company/culture/inclusion/erg-gitlab-pride/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

This group strives to connect employees at GitLab that are part of the LGBTQ+ community, or are allies, with professional and personal opportunities to meet others, speak at events, and share their lived experiences to improve and strengthen our community.

## Mission

To ensure that GitLab is proactive in supporting, retaining, and recruiting from the LGBTQ+ community. This group also aims to help develop and coordinate mentoring in the workplace.

## Leads
* [Alex Hanselka](https://about.gitlab.com/company/team/#ahanselka) 
* [Amber Lammers](https://about.gitlab.com/company/team/#amberlammers)

## Upcoming Events

## Additional Resources

- Sign up to get meeting invites by joining the [GitLab Pride Google Group](https://groups.google.com/a/gitlab.com/g/erg-pride/)
- [GitLab Pride Issue Board](https://gitlab.com/gitlab-com/pride-tmrg/)
