---
layout: markdown_page
title: 2020 GitLab KubeCon Quiz Sweepstakes 
description: "No purchase necessary to enter or win! First place prize is a Nintendo Switch. Learn more about this Developer Surveys Sweepstakes here!"
canonical_path: "/community/sweepstakes/2020-gitlab-kubecon-quiz/"
---

## KubeCon Europe 2020 GitLab Quiz - OFFICIAL RULES

NO PURCHASE NECESSARY TO ENTER OR WIN. MAKING A PURCHASE OR PAYMENT OF ANY KIND WILL NOT INCREASE YOUR CHANCES OF WINNING. VOID WHERE PROHIBITED OR RESTRICTED BY LAW. 

1.  PROMOTION DESCRIPTION: The KubeCon Europe 2020 GitLab Quiz (\"Prize Draw\") begins on August 17, 2020 at 12:00pm (CET) and ends on August 20, 2020 at 8:00pm (CET) (the \"Promotion Period\"). 

The sponsor of this Prize Draw is GitLab, Inc. ("Sponsor"). By participating in the Prize Draw, each Entrant unconditionally accepts and agrees to comply with and abide by these Official Rules and the decisions of Sponsor, which shall be final and binding in all respects. Sponsor is responsible for the collection, submission or processing of Entries and the overall administration of the Prize Draw. Entrants should look solely to Sponsor with any questions, comments or problems related to the Prize Draw. Sponsor may be reached by email at mwieling@gitlab.com during the Promotion Period. 

2.  ELIGIBILITY: Open to registrants of KubeCon Europe 2020 who are eighteen (18) or older (the \"Entrant\"). 

Sponsor, and their respective parents, subsidiaries, affiliates, distributors, retailers, sales representatives, advertising and promotion agencies and each of their respective officers, directors and employees (the \"Promotion Entities\"), are ineligible to enter the Prize Draw or win a prize. Household Members and Immediate Family Members of such individuals are also not eligible to enter or win. "Household Members" shall mean those people who share the same residence at least three months a year. "Immediate Family Members" shall mean parents, step-parents, legal guardians, children, step-children, siblings, step-siblings, or spouses. 

Any person located in a “Restricted Country” is not eligible. Restricted Countries are those countries with asset freeze sanctions from the European Union or the United States during the Promotion Period. 

This Prize Draw is subject to all applicable federal, state and local laws and regulations and is void where prohibited or restricted by law. 

3. PRIZES: 


Grand Prize: one (1) winner will receive one (1) PS4 Pro (approximate retail value or "ARV": €340)
Additional Prizes:
one (1) winner will receive one (1) Alienware 25 Gaming Monitor - AW2521HF (approximate retail value or "ARV": €300)
two (2) winners will each receive Training & Certification at Commit Virtual on August 26, 2020 (approximate retail value or “ARV”: €340) 


Only one prize per person and per household will be awarded. Gift cards and gift certificates are subject to the terms and conditions of the issuer. Prizes cannot be transferred, redeemed for cash or substituted by winner. Sponsor reserves the right in its sole and absolute discretion to award a substitute prize of equal or greater value if a prize described in these Official Rules is unavailable or cannot be awarded, in whole or in part, for any reason. The ARV of the prize represents Sponsor's good faith determination. That determination is final and binding and cannot be appealed. If the actual value of the prize turns out to be less than the stated ARV, the difference will not be awarded in cash. Sponsor makes no representation or warranty concerning the appearance, safety or performance of any prize awarded. Restrictions, conditions, and limitations may apply. Sponsor will not replace any lost or stolen prize items.
    
This Prize Draw is not open to persons in Restricted Countries and Prize will not be awarded and/or delivered to addresses within said locations. 

All federal, state and/or local taxes, fees, and surcharges are the sole responsibility of the prize winner. Failure to comply with the Official Rules will result in forfeiture of the prize.
    
4. HOW TO ENTER: Eligible individuals enter by correctly completing the quiz questions in GitLab's KubeCon Europe 2020 digital booth, accessible through the KubeCon Europe 2020 virtual expo hall.

Automated or robotic Entries submitted by individuals or organizations will be disqualified. Internet entry must be made by the Entrant. Any attempt by Entrant to obtain more than the stated number of Entries by using multiple/different email addresses, identities, registrations, logins or any other methods, including, but not limited to, commercial contest/Prize Draw subscription notification and/or entering services, will void Entrant's Entries and that Entrant may be disqualified. Final eligibility for the award of any prize is subject to eligibility verification as set forth below. All Entries must be posted by the end of the Promotion Period in order to participate. Sponsor's database clock will be the official timekeeper for this Prize Draw.
    
5. WINNER SELECTION: The Winner(s) of the Prize Draw will be selected in a random drawing from among all eligible Entries received throughout the Promotion Period. The random drawing will be conducted about one (1) week after the Promotion Period by Sponsor or its designated representatives, whose decisions are final. Odds of winning will vary depending on the number of eligible Entries received.

6. WINNER NOTIFICATION: Winner will be notified by email at the email address provided in the Entry Information approximately one (1) week after the random drawing. Potential Winner must accept a prize by email as directed by Sponsor within ninety (90) days of notification. Sponsor is not responsible for any delay or failure to receive notification for any reason, including inactive email account(s), technical difficulties associated therewith, or Winner’s failure to adequately monitor any email account.

Any winner notification not responded to or returned as undeliverable may result in prize forfeiture. The potential prize winner may be required to sign and return an affidavit of eligibility and release of liability, and a Publicity Release (collectively \"the Prize Claim Documents\"). No substitution or transfer of a prize is permitted except by Sponsor.

7. PRIVACY: Any personal information supplied by you will be subject to the privacy policy of the Sponsor posted at https://about.gitlab.com/privacy/. By entering the Prize Draw, you grant Sponsor permission to share your email address and any other personally identifiable information with the other Prize Draw Entities for the purpose of administration and prize fulfillment, including use in a publicly available Winners list.

If you wish to assert your rights to information, correction, deletion, restriction of processing, object to data processing or revoke your consent to data processing, please send an e-mail to gdpr-request@gitlab.com.

8. LIMITATION OF LIABILITY: Sponsor assumes no responsibility or liability for (a) any incorrect or inaccurate entry information, or for any faulty or failed electronic data transmissions; (b) any unauthorized access to, or theft, destruction or alteration of entries at any point in the operation of this Prize Draw; (c) any technical malfunction, failure, error, omission, interruption, deletion, defect, delay in operation or communications line failure, regardless of cause, with regard to any equipment, systems, networks, lines, satellites, servers, camera, computers or providers utilized in any aspect of the operation of the Prize Draw; (d) inaccessibility or unavailability of any network or wireless service, the Internet or website or any combination thereof; (e) suspended or discontinued Internet, wireless or landline phone service; or (f) any injury or damage to participant's or to any other person’s computer or mobile device which may be related to or resulting from any attempt to participate in the Prize Draw or download of any materials in the Prize Draw.

If, for any reason, the Prize Draw is not capable of running as planned for reasons which may include without limitation, infection by computer virus, tampering, unauthorized intervention, fraud, technical failures, or any other causes which may corrupt or affect the administration, security, fairness, integrity or proper conduct of this Prize Draw, the Sponsor reserves the right at its sole discretion to cancel, terminate, modify or suspend the Prize Draw in whole or in part. In such event, Sponsor shall immediately suspend all drawings and prize awards, and Sponsor reserves the right to award any remaining prizes (up to the total ARV as set forth in these Official Rules) in a manner deemed fair and equitable by Sponsor. Sponsor and Released Parties shall not have any further liability to any participant in connection with the Prize Draw.
9. GOVERNING LAW: This Prize Draw is governed by the laws of the Kingdom of the Netherlands.
  
10. WINNER LIST/OFFICIAL RULES: To view a copy of the Winner List or a copy of these Official Rules, visit https://about.gitlab.com/events/kubecon-amsterdam/. The winner list will be posted after winner confirmation is complete.

11. SPONSOR: GitLab, Inc., 268 Bush Street, #350, San Francisco, CA 94104, mwieling@gitlab.com


## KubeCon Europe 2020 GitLab Quiz - OFFICIËLE REGELS

ER IS GEEN AANKOOP NODIG OM MEE TE DOEN OF TE WINNEN. EEN AANKOOP OF BETALING ZAL UW KANSEN OM TE WINNEN NIET VERGROTEN. DEELNAME IS ONGELDIG WAAR DAT VERBODEN OF BEPERKT IS DOOR DE WET. 

1.  PROMOTIE OMSCHRIJVING: De KubeCon Europe 2020 GitLab Quiz (\"Prijstrekking\") begint op 17 augustus 2020 om 12.00 uur (CET) en eindigt op 20 augustus 2020 om 20.00 uur (CET) (de \"Promotieperiode\"). 

De Sponsor van deze Prijstrekking is GitLab, Inc. ("Sponsor"). Door deel te nemen aan de Prijstrekking accepteert en stemt elke deelnemer ermee in zich onvoorwaardelijk te houden aan de Officiële Regels en de beslissingen van de Sponsor die in alle gevallen definitief en bindend zijn. De Sponsor is verantwoordelijk voor het verzamelen, indienen of verwerken van de Inzendingen en het algemene beheer van de Prijstrekking. Deelnemers dienen zich uitsluitend tot de Sponsor te wenden met vragen, opmerkingen of problemen met betrekking tot de Prijstrekking. De Sponsor is tijdens de Promotieperiode per e-mail bereikbaar op mwieling@gitlab.com. 

2.  VOORWAARDEN TOELATING: Degenen die geregistreerd zijn bij KubeCon Europe 2020 en achttien (18) jaar of ouder zijn (de \"Deelnemers\"). 

De Sponsor en hun respectievelijke moederonderneming, dochterondernemingen, gelieerde ondernemingen, distributeurs, detailhandelaars, vertegenwoordigers, reclame- en promotiebureaus en elk van hun respectievelijke functionarissen, directeuren en werknemers (de \"Promotie-entiteiten\") komen niet in aanmerking voor de Prijstrekking of het winnen van een prijs. Gezinsleden en directe familieleden van dergelijke personen komen ook niet in aanmerking om deel te nemen of winnen. "Gezinsleden" zijn personen die ten minste drie maanden per jaar dezelfde woning delen. "Onmiddellijke familieleden" zijn ouders, stiefouders, wettelijke voogden, kinderen, stiefkinderen, broers en zussen, stiefbroers of zussen, of echtgenoten. 

Geen enkele persoon in een "Land waarvoor een Restrictie geldt " komt in aanmerking. Landen waarvoor een Restrictie geldt zijn landen waarvoor, tijdens de Promotieperiode, sancties gelden inzake het bevriezen van tegoeden van de Europese Unie of de Verenigde Staten. 

Deze Prijstrekking is onderworpen aan alle toepasselijke federale, staats- en lokale wet- en regelgeving en is ongeldig indien wettelijk verboden of beperkt. 

3. PRIJZEN: 


Hoofdprijs: één (1) winnaar ontvangt één (1) PS4 Pro (geschatte waarde: € 340)
Extra prijzen:
één (1) winnaar ontvangt één (1) Alienware 25 Gaming Monitor - AW2521HF (geschatte waarde: €300)
twee (2) winnaars ontvangen elk een training en certificering op de Commit Virtual op 26 augustus 2020 (geschatte waarde: € 340) 


Er wordt slechts één prijs per persoon en per huishouden toegekend. Cadeaubonnen en vouchers zijn onderworpen aan de voorwaarden van degene die ze uitgeeft. Prijzen kunnen niet worden overgedragen, ingewisseld voor geld, of door de winnaar worden omgewisseld. De Sponsor behoudt zich het recht voor om naar eigen goeddunken een vervangende prijs van gelijke of grotere waarde toe te kennen indien een prijs die wordt beschreven in deze Officiële Regels niet beschikbaar is of om welke reden dan ook, geheel of gedeeltelijk, niet kan worden toegekend. De geschatte waarde van de prijs is op goed vertrouwen vastgesteld door de Sponsor. Deze vaststelling is definitief en bindend en er kan geen beroep tegen worden aangetekend. Als de werkelijke waarde van de prijs lager blijkt te liggen dan de vermelde geschatte waarde wordt het verschil niet in contanten uitgekeerd. De Sponsor geeft geen verklaring of garantie met betrekking tot het uiterlijk, de veiligheid of de prestaties van een toegekende prijs. Er kunnen beperkingen, voorwaarden en voorschriften van toepassing zijn. De Sponsor vervangt geen verloren of gestolen prijs items.
    
Deze Prijstrekking staat niet open voor personen in Landen waarvoor een Restrictie geldt en de Prijs wordt niet toegekend aan en/of afgeleverd op adressen op de genoemde locaties.	 

Alle federale, staats- en/of lokale belastingen, toeslagen en heffingen behoren tot de exclusieve verantwoordelijkheid van de prijswinnaar. Als de officiële regels niet worden nageleefd vervalt de prijs.
    
4. HOE U MEE KUNT DOEN: personen die in aanmerking komen kunnen meedoen door de quiz vragen in GitLab's KubeCon Europe 2020 online booth in de virtuele expohal van KubeCon Europe 2020 correct te beantwoorden.

Geautomatiseerde of door robots gegenereerde inzendingen die door individuen of organisaties zijn ingediend worden gediskwalificeerd. De toegang tot het internet moet door de Deelnemer worden gemaakt. Elke poging van de Deelnemer om meer dan het vermelde aantal inzendingen te realiseren middels meerdere/verschillende e-mailadressen, identiteiten, registraties, logins of andere methoden, inclusief maar niet beperkt tot, commerciële abonnement melding en toegangsdiensten voor wedstrijden/Prijstrekkingen, maken de inzendingen van de Deelnemer ongeldig en kunnen tot diskwalificatie leiden. De uiteindelijke toelatingsvoorwaarden voor een prijs is afhankelijk van de toelating controle zoals hieronder uiteengezet. Alle inzendingen moeten aan het einde van de Promotieperiode worden gepost om deel te mogen nemen. De klok van de database van de Sponsor is de officiële tijdwaarnemer voor deze Prijstrekking.
    
5. BEPALING WINNAAR: De Winnaar(s) van de Prijstrekking(en) wordt/worden door middel van een willekeurige trekking bepaald uit alle in aanmerking komende Inzendingen die tijdens de Promotieperiode zijn ontvangen. De willekeurige trekking wordt ongeveer één (1) week na de Promotieperiode uitgevoerd door de Sponsor of diens aangewezen vertegenwoordigers, wiens beslissingen definitief zijn. De kans om te winnen is afhankelijk van het aantal in aanmerking komende inzendingen dat is ontvangen.

6. KENNISGEVING WINNAAR: De winnaar wordt ongeveer één (1) week na de willekeurige trekking per e-mail op de hoogte gesteld op het e-mailadres dat is opgegeven bij de Inschrijvingsinformatie. De Potentiële Winnaar moet een prijs binnen negentig (90) dagen na kennisgeving per e-mail accepteren zoals aangegeven door de Sponsor. De Sponsor is niet verantwoordelijk voor enige vertraging of het niet ontvangen van een kennisgeving om welke reden dan ook, inclusief inactieve e-mailaccount(s), technische problemen die daarmee verband houden, of het feit dat de Winnaar zijn/haar e-mailaccount niet naar behoren controleert.

Elke kennisgeving aan een Winnaar die niet beantwoord wordt of als onbestelbaar terugkeert kan resulteren in verbeurdverklaring van de prijs. De potentiële prijswinnaar moet mogelijk een verklaring inzake de toelatingsvoorwaarden en een vrijwaring van aansprakelijkheid ondertekenen en terugsturen, alsmede Vrijgave voor Publiciteit (gezamenlijk "de \Prijsuitreikingsdocumenten\"). Vervanging of overdracht van een prijs is alleen toegestaan door de Sponsor.

7. PRIVACY: Alle persoonlijke informatie die door u wordt verstrekt is onderworpen aan het privacybeleid van de Sponsor, gepubliceerd op https://about.gitlab.com/privacy/. Door deel te nemen aan de Prijstrekking geeft u de Sponsor toestemming om uw e-mailadres en andere persoonlijk identificeerbare informatie te delen met andere Entiteiten die bij de Prijstrekking betrokken zijn met het oog op administratieve doeleinden en de uitreiking van de prijs, inclusief opname in een publiekelijk beschikbare lijst met Winnaars.

Als u uw rechten op informatie, correctie, verwijdering en beperking van verwerking wilt laten gelden, bezwaar wilt maken tegen gegevensverwerking of uw toestemming voor gegevensverwerking wilt intrekken, stuur dan een e-mail naar gdpr-request@gitlab.com.

8. BEPERKING AANSPRAKELIJKHEID: De Sponsor aanvaardt geen verantwoordelijkheid of aansprakelijkheid voor (a) enige onjuiste of onnauwkeurige deelname-informatie of voor enige foutieve of mislukte elektronische gegevensoverdracht; (b) ongeoorloofde toegang tot of diefstal, vernietiging of wijziging van Inzendingen op enig moment tijdens de uitvoering van deze Prijstrekking; (c) enige technische storing, gebrek, fout, weglating, onderbreking, verwijdering, defect, vertraging in de werking of storing van communicatielijnen, ongeacht de oorzaak, met betrekking tot apparatuur, systemen, netwerken, lijnen, satellieten, servers, camera’s, computers of gebruikte providers met betrekking tot enig aspect van de werking van de Prijstrekking; (d) ontoegankelijkheid of het niet beschikbaar zijn van een netwerk of draadloze dienst, het internet of de website of een combinatie ervan; (e) opgeschorte of beëindigde internet-, draadloze of vaste telefoondiensten; of (f) letsel of schade aan de computer van een Deelnemer of aan de computer of het mobiele apparaat van een andere persoon die verband zou kunnen houden met of het gevolg is van een poging om deel te nemen aan de Prijstrekking of het downloaden van materiaal met betrekking tot de Prijstrekking.

Als, om welke reden dan ook, de Prijstrekking niet kan worden uitgevoerd zoals gepland om redenen die onder meer kunnen bestaan uit, maar niet beperkt zijn tot, infectie door een computervirus, manipulatie, ongeautoriseerde tussenkomst, fraude, technische storingen of andere oorzaken die de administratie, veiligheid, eerlijkheid, integriteit of het juiste verloop van deze Prijstrekking kunnen aantasten of beïnvloeden, behoudt de Sponsor zich het recht voor om naar eigen goeddunken de Prijstrekking geheel of gedeeltelijk te annuleren, beëindigen, wijzigen of op te schorten. In dat geval zal de Sponsor alle trekkingen en prijsuitreikingen onmiddellijk opschorten en behoudt de Sponsor zich het recht voor om eventuele resterende prijzen (tot de totale geschatte winkelwaarde zoals uiteengezet in deze Officiële Regels) toe te kennen op een manier die door de Sponsor als eerlijk en billijk wordt aangemerkt. De Sponsor en de Vrijgestelde Partijen zijn niet aansprakelijk jegens een Deelnemer in verband met de Prijstrekking.

9. TOEPASSELIJK RECHT: Deze Prijstrekking valt onder de wetten van het Koninkrijk der Nederlanden.
  
10. WINNAARSLIJST/OFFICIËLE REGELS: Ga naar https://about.gitlab.com/events/kubecon-amsterdam/ om een exemplaar van de Winnaarslijst of een exemplaar van deze Officiële Regels te bekijken. De Winnaarslijst wordt gepost nadat het bevestigen van de Winnaar is voltooid.

11. SPONSOR: GitLab, Inc., 268 Bush Street, #350, San Francisco, CA 94104, mwieling@gitlab.com




